/*
Name :  Input.h
Author: Amit Malyala 
Description:
This module gets input from the user for entering a program. End of of program is indicated by a '.' at the
beginning of a newline.
License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.
Notes:
Bug and revisioin history;
0.1 Initial version
*/

#ifndef INPUT_H
#define INPUT_H

#include "vector"
#include "iostream"

// Get input from user.
void GetInput(std::vector <unsigned char> & s);

#endif
