#include "iostream"
#include <vector>
#include "std_types.h"
#include "Commentsprocessing.h"
/*

License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.
*/

SINT32 main(void)
{
   std::vector <UINT8> Prog;
   std::cout << "Enter a C++ program, enter '.' at beginning of a newline to end input." << std::endl;
   GetInput(Prog);
   std::cout << "You entered: " << std::endl;
   DisplayOutput(Prog);
   InitPreprocessor(Prog);
   return 0;
}