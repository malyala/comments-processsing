/*
Name: CommentsProcessing.h
Author: Amit Malyala
License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.
*/

#ifndef COMMENTSPROCESSING_H
#define COMMENTSPROCESSING_H

#include "iostream"
#include "vector"
#include "Commentsprocessing.h"
#include "Input.h"
#include "Error.h"
#include "std_types.h"

// Number of preprocessor passes
#define NUMBER_OF_PREPROCESSOR_PASSES 2

#define FIRST_PASS 0x01
#define SECOND_PASS 0x02
#define THIRD_PASS 0x03
#define FOURTH_PASS 0x03

//  Initialize Preprocessor
void InitPreprocessor(std::vector <unsigned char> & Prog);

// Remove comments from the program which is stored in a std::vector
unsigned int RemoveCppComments(std::vector <unsigned char> & s);

// Display output
void DisplayOutput(const std::vector <UINT8> & s);
#endif
