/*
Name :  Error.cpp
Author: Amit Malyala
Description:
This module produces errors when called from different modules. The interpreter module would check errors 
stored by this module. The error module would add all errors into a std::vector of std::strings . 
The interpreter checks if this vector is empty before executing. Errors listed in this vector of std::strings would be 
displayed after stopping program execution.

License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.

Notes:
Bug and revisioin history;
0.1 Initial version
0.2 Added line numbers at which Errors occured
0.3 Changed some function parameters to const data type & 
*/
#include "Error.h"
/*
Component Function: static std::string itostring(UINT32 a)
Arguments:  None
returns: None
Description:
Convert a decimal UINT32eger to std::string 
Version : 0.1
*/
static std::string itostring(const UINT32& a);

/*
Component Function: static UINT32 getNumofDecDigit(UINT32 Number)
Arguments:  None
returns: Number of digits
Description:
Returns Number of digits in a UINT32eger
Version : 0.1
*/
static UINT32 getNumofDecDigit(UINT32 Number);
/*
Component Function: static char getStrChar(UINT32 digit)
Arguments:  None
returns: a digit as a char
Description:
Get a character from  digit input number 
Version : 0.1
*/
static char getStrChar(const UINT32& digit);

// Vector of strings to store Errors with their error codes
static std::vector <std::string> ErrorsList;

// Comment to integrate with all modules.
#if(0)
int main (void)
{
	ErrorTracer("Missing Comment or incorrect number of comments",MISSING_COMMENT,1);
	ErrorTracer("Incorrect Variable definition", INCORRECT_VARIABLE_DEFINITION,2);
	ErrorTracer("Incorrect variable declaration",INCORRECT_VARIABLE_DECLARATION,3);
	ErrorTracer("Missing or incorrect function arguments", MISSING_FUNCTION_ARGS,4);
	ErrorTracer("Incorrect function definition",INCORRECT_FUNCTION_DEFINITION,5);
    ErrorTracer("Illegal character",ILLEGAL_CHARACTER,6);
	PrintErrors();
	return 0;	
}
#endif

/*
Component Function: void ErrorTracer(std::string Error, UINT32 ErrorCode)
Arguments:  Error from module string and Error code 
returns: None
Description:
Adds Error from module with error code to a vector of strings which contains total errors ever captured.
Version : 0.2
*/
void ErrorTracer (const std::string& ErrorStr, UINT32 ErrorCode, UINT32 LineNumber)
{
	std::string Error = "Error:";
	switch(ErrorCode)
	{
		case MISSING_COMMENT:
		Error += "0x01 ";
		break;
		
		case INCORRECT_VARIABLE_DECLARATION:
		Error += "0x02 ";
		break;
		
		case INCORRECT_VARIABLE_DEFINITION:
		Error += "0x03 ";
		break;
		
		case MISSING_FUNCTION_ARGS:
		Error += "0x04 ";
		break;
		
		case INCORRECT_FUNCTION_DEFINITION:
		Error += "0x05 ";
		break;
		
		case ILLEGAL_CHARACTER:
		Error += "0x06";
		break;
		
		default:
		Error += "0x00 ";
		break;
	}
	Error+=ErrorStr;
	Error+= " at line number: ";
	Error+= itostring(LineNumber);
	ErrorsList.push_back(Error);
}

/*
Component Function: CheckforErrors(void)
Arguments:  None
returns: None
Description:
Checks if errors are present and returns errors present or not present.
Version : 0.1
*/
UINT32 CheckforErrors(void)
{
	// if the vector is not empty, print all errors return an error code 
	UINT32 NumofErrors=ErrorsList.size();
	if (NumofErrors)
	{
		return COMPILATION_ERRORS;
		
	}
	// if the vector empty then return a no error code.
	else
	{
		return NO_ERRORS;
	}
}
/*
Component Function: void PrintErrors(void)
Arguments:  None
returns: None
Description:
Displays errors present on a console.
Version : 0.1
*/

void PrintErrors(void)
{
	UINT32 i=0;
	// Print all errors 
	for ( i=0;i<ErrorsList.size();i++)
	{
		std::cout <<std::endl<<ErrorsList[i];
	}
}

/*
Component Function: static std::string itostring(UINT32 a)
Arguments:  UINT32 to be converted to std::string
returns: None
Description:
Convert a decimal UINT32eger to std::string 
Version : 0.1
*/
static std::string itostring(const UINT32& a)
{
   std::string b;
   UINT32 NumofDigits = getNumofDecDigit(a);
   
    switch(NumofDigits)
    {
    	case 0:
    	break;	
    	
        case 1:
        b+=getStrChar(a);
        break;

        case 2:
        b+= getStrChar(a/10);
        b+= getStrChar(a%10);
        
        break;

        case 3:
        b+= getStrChar(a/100);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
               
        break;

        case 4:
        b+= getStrChar(a/1000);
        b+= getStrChar((a/100)%10);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
        
        break;

        case 5:
        b+= getStrChar(a/10000);
        b+= getStrChar((a/1000)%10);
        b+= getStrChar((a/100)%10);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
        
        break;

        case 6:
        b+= getStrChar(a/100000);
        b+= getStrChar((a/10000)%10);
        b+= getStrChar((a/1000)%10);
        b+= getStrChar((a/100)%10);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
        
        break;

  
        case 7:
        b+= getStrChar(a/1000000);
        b+= getStrChar((a/100000)%10);
        b+= getStrChar((a/10000)%10);
        b+= getStrChar((a/1000)%10);
        b+= getStrChar((a/100)%10);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
        
        break;

        case 8:
        b+= getStrChar(a/10000000);
        b+= getStrChar((a/1000000)%10);
        b+= getStrChar((a/100000)%10);
        b+= getStrChar((a/10000)%10);
        b+= getStrChar((a/1000)%10);
        b+= getStrChar((a/100)%10);
        b+= getStrChar((a/10)%10);
        b+= getStrChar(a%10);
        
        break;

        default:
        break;
    }
    return b;
}

/*
Component Function: static UINT32 getNumofDecDigit(UINT32 Number)
Arguments:  Number to detect digits
returns: Number of digits
Description:
Returns number of digits in a UINT32eger
Version : 0.1
*/

static UINT32 getNumofDecDigit(UINT32 Number)
{
   UINT32 NumofDigits=0;
   //Number= (Number>0)? Number: -Number;
      while(Number)
      {
          Number/=10;
          NumofDigits++;
      }
    return NumofDigits;
}


/*
Component Function: static char getStrChar(UINT32 digit)
Arguments:  digit as UINT32
Returns: a digit as a char
Description:
Get a character from  digit input number 
Version : 0.1
*/
static char getStrChar(const UINT32& digit)
{
    char  StrChar = ' ';
    switch (digit)
    {
        case 0:
        StrChar = '0';
        break;

        case 1:
        StrChar = '1';
        break;

        case 2:
        StrChar = '2';
        break;

        case 3:
        StrChar = '3';
        break;

        case 4:
        StrChar = '4';
        break;

        case 5:
        StrChar = '5';
        break;

        case 6:
        StrChar = '6';
        break;

        case 7:
        StrChar = '7';
        break;

        case 8:
        StrChar = '8';
        break;

        case 9:
        StrChar = '9';
        break;

        default:
        StrChar ='0';
        break;
    }
    return StrChar;
}