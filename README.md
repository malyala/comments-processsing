## Comments processing for removing C-style and C++ comments.
This program  detects and removes C-style and C++ comments from a C++ program. Recursion is used to remove comments. 

The project compiles with TDM-GCC under ISO C++11 and Visual studio 2017 under ISO C++14.

Nice to have:
Detecting nested c-style comments and removing them.
Detecting comments within " " in printf and std::cout statements.

Author: Amit Malyala
Email: amit at malyala.net


