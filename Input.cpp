/*
Name :  Input.cpp
Author: Amit Malyala 
Description:
This module gets input from the user for entering a program. End of of program is indicated by a '.' at the
beginning of a newline.
License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.
Notes:
Bug and Version history;
0.1 Initial version
0.2 Implemented a end of input flag
*/

#include "Input.h"
// Get input from user.
 void GetInput(std::vector <unsigned char> & s)
{
   unsigned char c = 0;
   unsigned char previouschar = ' ';
   bool EndofInput = false;
   /*
   Read input until a '.' is entered on the beginning
   of a newline.When reading a program from a input file, you would read
   until EOF is reached.
   */
   while(EndofInput== false)
   {
      c =std::cin.get();
      s.push_back(c);
      if(previouschar == '\n' && c == '.')
      {
          // End of input marker is '.' after the char '\n'.
         EndofInput =true;
      }
      previouschar=c;
   }
}
