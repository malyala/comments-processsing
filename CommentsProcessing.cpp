/*
Name :  CommentsProcessing.cpp
Author: Amit Malyala
Description:
This is a partial preprocessor module for a partial C++ preprocessor which detects c-style and C++ comments in a c++ program
and removes them.

License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.

Notes:
Version and bug history:
-------------------------
0.1 Initial version
0.2 Made Some functions static
0.3 Changed BeginCommentsRead() function to include BeginComments Read and EndComments Read parameters.
0.4 Wrote code so that end comment is now detected without a begin comment
0.5 Changed order of comment parsing, // Comments are detected first.
0.6 Wrote code for detecting missing end comment
0.7 Implemented code to detect missing / token in a double slash comment.
0.8 Implemented multiple passes through the preprocessor while processing comments.

Known issues: None
Unknown issues: None
*/
#include "Commentsprocessing.h"
/*
Component Function: static UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments, UINT32& EndComments)
Arguments: Vector s which contains program text,UINT32 i for position in the program
returns : Position of the program last read.
Description:
Read through a comment, parse text under comments and erase.
Call this function recursively each time a / * token is found.
This is a function to parse comments and nested comments.
Version : 0.2
*/
static UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments, \
UINT32& EndComments, UINT32& LineNumber, UINT32& CurrrentPass);

// This function should be commented in the main project.
// Diplay output with line numbers.
 void DisplayOutput(const std::vector <UINT8> & s)
{
  UINT32 i=0;
  UINT32 Linenumber =1;
  UINT8 Linenumberprinted= false;
  bool EndofInput=false;
  while((i+1) < s.size() && EndofInput == false)
  {
    if(Linenumberprinted==false)
    {
        std::cout << Linenumber << ": " ;
        Linenumberprinted=true;
    }

    if ( s[i] == '\n' && s[i+1] == '.')
     {
         EndofInput=true;
     }
     else
     {
         std::cout << s[i];
     }
     if (s[i]== '\n')
     {
         Linenumber++;
         Linenumberprinted=false;
     }
     i++;
   }
}

/*
Component Function: InitPreprocessor(std::vector <UINT8> & Prog)
Arguments: Reference to vector Prog which contains the C++ program text
returns: None
Description:
Initialize Preprocessor and start to remove comments.
Bug and Version history :
         0.1 Initial version
*/
void InitPreprocessor(std::vector <UINT8> & Prog)
{
   UINT32 PreProcessorErrors=false;
   std::cout << "\nPreprocessor is removing comments: " << std::endl;
   PreProcessorErrors=RemoveCppComments(Prog);
   std::cout << "\nAfter preprocessing: " << std::endl;

   if (PreProcessorErrors)
   {

   	std::cout<<"\nErrors Found. Please see above!";

   }
   else
   {
   	DisplayOutput(Prog);
   }
}
/*
Component Function: RemoveCppComments(std::vector <UINT8> & s)
Arguments: Reference to vector s which contains the C++ program text
returns: None
Description:
Read through a comment, parse text under comments and erase.
Call this function recursively each time a / * token is found.
This is a function to parse comments and nested comments.
Bug and Version history :
0.1 Initial version
0.2 Added nested comment support
0.3 Fixed a feature in which end comment is now detected without a begin comment.
*/
UINT32 RemoveCppComments(std::vector <UINT8> & s)
{
    // Implement missing comment detection for /* or */ tokens
    UINT32 i = ZERO;
    static UINT32 BeginComments=ZERO;
    static UINT32 EndComments=ZERO;
    UINT32 LineNumber=ONE;
    UINT32 SlashErrors=NO_ERRORS;
    UINT32 CurrentPass=FIRST_PASS;
    UINT32 NumOfPasses=ONE;
    UINT32 ErrorsFound=false;
   // The number of preprocessor passes can be increased to do enhanced preprocessing with additional features.
    for (NumOfPasses=FIRST_PASS;NumOfPasses<=NUMBER_OF_PREPROCESSOR_PASSES && ErrorsFound==false;NumOfPasses++)
    {

	 if (NumOfPasses==FIRST_PASS)
	 {
	 	CurrentPass=FIRST_PASS;
	 	//std::cout << "\nNumber of preprocessor passes" << NumOfPasses;

	 }
	 else if (NumOfPasses==SECOND_PASS)
	 {
	 	CurrentPass=SECOND_PASS;
	 	//std::cout << "\nNumber of preprocessor passes" << NumOfPasses;
	 }

	 i=0; // Initialize loop counter
	 // First pass through the program to detect and  remove '//' comments
    while(i+1 < s.size())
    {

      // Remove // comments
          if(s[i] == '/' && s[i+1]== '/')
          {

			  //#if(0)
              // Diagnostic messages.
              // std::cout << "Begin comment // detected. " << std::endl;
             // #endif

              while(s[i] != '\n' && i<s.size())
              {

                 // #if(0)
                  // Diagnostic message
                  //std::cout<< " Erase byte " << std::endl;
                 // #endif

                 if (CurrentPass==SECOND_PASS)
                 {
                 	s[i] = ' '; // Erase all bytes until end of line.

				 }

                 i++;
              }
          }
		  // Skip C-style comments in detecting // comments
		  else if ( (s[i]=='/' && s[i+1] == '*'))
		  {
		  	i++;  // Skip /
		  	i++;  // Skip *
		  }
		  else if ( (s[i]=='*' && s[i+1] == '/'))
		  {
		  	i++; // Skip *
		  	i++; // Skip /
		  }

		  else if (s[i]=='/'&& s[i+1] !='/' )
          {
          	SlashErrors++;
          	ErrorTracer("Missing / token in a // type comment",MISSING_COMMENT,LineNumber);
          }

          //#endif
		  if (s[i]=='\n')
		  {
		  	LineNumber++;
		  }

       i++;
    }

    //#if(0)
    i=0;  // Reset loop counter.
    // Restart the scan for /* */ comments
	LineNumber=1;
      // Other pass through the program to detect /**/ comments
    while( i+1 <= s.size())
    {
      // Code to detect /*  comment
      if(s[i]=='/' && s[i+1]== '*')
      {

        //#if(0)
        // Diagnostic messages.
        //std::cout << "Begin comment /* detected. " << std::endl;
        BeginComments++;
       // #endif
        i=BeginCommentRead(s,i,BeginComments,EndComments,LineNumber,CurrentPass);


        // Now detect comments after comments such as /*/**/*//**/
       if (s[i] =='/' && s[i+1] =='*')
       {
       	 BeginComments++;

         continue;
       }

      }
      // Detect if */ Comment starts first without a begin comment /*
      else if (s[i]== '*' && s[i+1] == '/')
      {
      	EndComments++;
      	ErrorTracer("Missing /* token in a comment ending",MISSING_COMMENT,LineNumber);
	  }

	  	if (s[i] == '\n')
    	{
    		LineNumber++;
		}

      i++;
    }
    //#endif

   // Diagnostic messages.
   //#if(0)
  // std:: cout << "Comments parsing finished " << std::endl;
  // std::cout << "Total number of comments" << std::endl;
   //std::cout <<"Begin Comments: " << BeginComments << " End Comments: " << EndComments;
   if ((CheckforErrors()) && (CurrentPass==FIRST_PASS))
   {
   	    ErrorsFound=true;
		PrintErrors();
   }
   //#endif

   }

   return ErrorsFound;
}
/*
Component Function: static UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i,  \
UINT32& BeginComments, UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass)
Arguments: Vector s which contains program text,UINT32 i for position in the program, Number of Begin comments,
           Num of End comments, Line number )
Returns : Position of the program last read.
Description:
Read through a comment, parse text under comments and erase.
Call this function recursively each time a / * token is found.
This is a function to parse comments and nested comments.
Version : 0.2
Notes:
 */
static UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments,\
UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass)
{
    static UINT32 ReadPos=ZERO;
    static UINT32 NestedComments=ZERO;
    UINT32 EndCommentFound=false;
    UINT32 RecentBeginCommentLine=ZERO;

    // #if(0)
    //std::cout << "Erasing  /" << std::endl;
    // #endif

    if (CurrentPass==SECOND_PASS)
     {
         	s[i] = ' ';  // Erase /
     }
     i++;

    // #if(0)
    //std::cout << "Erasing  *" << std::endl;
    // #endif
    if (CurrentPass==SECOND_PASS)
     {
         	s[i] = ' ';  // Erase *

	 }
    i++;
    RecentBeginCommentLine=LineNumber;
    while(i+1 <s.size())
     // Read until */ token
     {
      // If a nested comment is found, start reading in that comment.
      if (s[i]=='/' && s[i+1]=='*')
      {
        // #if(0)
        // Diagnostic messages.
        //std::cout << "Begin comment /* detected. " << std::endl;
        BeginComments++;
        // #endif
        ReadPos=BeginCommentRead(s,i,BeginComments,EndComments,LineNumber,CurrentPass);
        // Use nested comment count to to erase until last */ in a nested comments set.
        NestedComments++;
        // #if(0)
        // Diagnostic messages.
        //std::cout << "Nested comments detected: " << NestedComments << std::endl;
        // #endif
        i=ReadPos;
      }
        // For now, we will assume there are no \n chars in cout " " strings
       if (s[i]=='\n' )
       {
           i++;
           LineNumber++;
           continue;
       }

       if ((s[i] =='*' && s[i+1] =='/'))
       {
         EndComments++;
         EndCommentFound=true;
         break;
	   }
	    // #if(0)
        // Diagnostic message
        //std::cout<< "Erasing byte " << std::endl;
        //#endif
       if (CurrentPass==SECOND_PASS)
       {
         	s[i] = ' ';  // Erase all bytes before */ token

	   }
	   i++;
    }
    // Code for detecting missing */ token in a comment
    if (EndCommentFound==false)
    {
    	ErrorTracer("Missing */ token in a comment starting",MISSING_COMMENT, RecentBeginCommentLine);
	}


    if ( NestedComments >0 )
    {
		if ((i+1) < s.size())

		{
			while (NestedComments > 0)
			{
				while ((i < s.size()) && s[i] != '*' && s[i + 1] != '/')
				{
					if (s[i] == '\n')
					{
						i++;
						LineNumber++;
					}
					else
					{
						// #if(0)
						 // Diagnostic message.
						 //std::cout<< " Erasing byte " << std::endl;
						// #endif
						if (CurrentPass == SECOND_PASS)
						{
							s[i] = ' ';  // Erase all bytes before */ token
						}
						i++;
					}
				}

				// #if(0)
				 // Diagnostic messages.
				if (s[i] == '*' && s[i + 1] == '/')
				{
					//std::cout <<"End nested comment */ detected " << std::endl;

					if (CurrentPass == SECOND_PASS)
					{
						s[i] = ' '; // Erase *

					}
					i++;
					// #if(0)
					 // Diagnostic message
					// std::cout << "Erasing  /" << std::endl;
					// #endif
					if (CurrentPass == SECOND_PASS)
					{
						s[i] = ' '; // Erase /

					}
					i++;
					EndCommentFound = true;
				}
				// #endif
				 // Erase */ token within nested comments
				// #if (0)
				 // Diagnostic message
				 //std::cout << "Erasing  *" << std::endl;
				// #endif


				NestedComments--;
			}
		}
    }
    else
    {
       //#if(0)
       // Diagnostic messages.
       //std::cout << "In outer nested comment block or in a single comment block"  << std::endl;
      // #endif

	while(i+1 <s.size())
    // Read until */ token
     {
       // For now, we will assume there are no \n chars in cout " " strings
       if (s[i]=='\n' )
       {
           i++;
           continue;
       }

       else if (s[i] == '*' && s[i+1]== '/')
       {
       	    //std::cout <<"End comment */ detected " << std::endl;
       	    break;
       }
      // #if(0)
       // Diagnostic messages
       //std:: cout << "Erasing byte in outer nested or single inner block" << std::endl;
      // #endif

      if (CurrentPass==SECOND_PASS)
       {
         	s[i] = ' ';  // Erase all bytes before */ token

	   }
       i++;
     }

    //  #if(0)
     // Diagnostic messages.
     if (s[i]=='*' && s[i+1]=='/')
     {
        //std::cout <<"Outer End comment */ detected " << std::endl;

	   //std::cout << "Erasing  *" << std::endl;
	   if (CurrentPass==SECOND_PASS)
       {
         	s[i]=' '; // Erase *

	   }
	   i++;
     //std::cout << "Erasing  /" << std::endl;
      if (CurrentPass==SECOND_PASS)
       {
         	s[i]=' '; // Erase /

	   }
	   i++;

     }
     // #endif

      //#if(0)
      // Diagnostic message.
      //std::cout << "Erasing  *" << std::endl;
     // #endif
    }
   return i;
}