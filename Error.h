/*
Name :  Error.h
Author: Amit Malyala
Description:
This module produces errors when called from different modules. 
License:
This program is part of Comments processing project licensed with BSD 2 Clause license. 
Please see License.txt for details.

Notes:
Bug and revisioin history;
0.1 Initial version
*/
#ifndef ERROR_H
#define ERROR_H
#include "string"
#include "vector"
#include "iostream"
#include "std_types.h"
// Error codes for various syntax errors.
#define MISSING_COMMENT 0x01
#define INCORRECT_VARIABLE_DEFINITION 0x02
#define INCORRECT_VARIABLE_DECLARATION 0x03
#define MISSING_FUNCTION_ARGS 0x04
#define INCORRECT_FUNCTION_DEFINITION 0x05
#define ILLEGAL_CHARACTER 0x06
// Other error codes
#define COMPILATION_ERRORS 0x121
#define NO_ERRORS 0x00

/*
Component Function: void ErrorTracer(std::string Error, UINT32 ErrorCode)
Arguments:  Error from module string and Error code 
returns: None
Description:
Adds Error from module with error code to a vector of strings which contains total errors ever captured.
Version : 0.2
*/
void ErrorTracer (const std::string& ErrorStr, UINT32 ErrorCode, UINT32 LineNumber);

// Check for Errors during compilation.
UINT32 CheckforErrors(void);

// Dispaly errors
void PrintErrors(void);
#endif
